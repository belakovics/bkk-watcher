package bkkwatcher.statistics.business;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

public class Test_CalculateFrequency {

	private CalculateFrequency cf;
	
	@Before
	public void setUp() {
		cf = new CalculateFrequency();
	}
	
	@Test
	public void on_one_element_list_gives_back_itself_with_one_freq() {
		
		ArrayList<Object> a = new ArrayList<Object>();
		a.add(42);
		
		HashMap<Object, Integer> actual = cf.sortByFrequency(a);
		
		assertEquals("Size is", 1, actual.size());
		assertTrue(actual.containsKey(42));
		assertTrue(actual.get(42) == 1);
	}

	@Test
	public void on_two_element_list_with_different_values_gives_back_the_values_with_one_freqs() {
		
		ArrayList<Object> a = new ArrayList<Object>();
		a.add(42);
		a.add(13);
		
		HashMap<Object, Integer> actual = cf.sortByFrequency(a);
		
		assertEquals( 2, actual.size());
		assertTrue(actual.get(13) ==  1);
		assertTrue(actual.get(42) == 1);		
	}
	
	@Test
	public void on_two_element_list_with_same_values_gives_back_the_value_with_freq_of_two() {
		
		ArrayList<Object> a = new ArrayList<Object>();
		a.add(42);
		a.add(42);
		
		HashMap<Object, Integer> actual = cf.sortByFrequency(a);
		
		assertEquals( 1, actual.size());
		assertTrue(actual.get(42) == 2);		
	}
	
	@Test
	public void on_longer_list_returns_with_the_elems_and_right_freqs() {
		
		ArrayList<Object> a = new ArrayList<Object>();
		a.add(42);
		a.add(42);
		a.add(13);
		a.add(13);
		a.add(13);
		
		HashMap<Object, Integer> actual = cf.sortByFrequency(a);
		
		assertEquals( 2, actual.size());
		assertTrue (2 ==  actual.get(42));
		assertTrue (3 ==  actual.get(13));
	}
}
