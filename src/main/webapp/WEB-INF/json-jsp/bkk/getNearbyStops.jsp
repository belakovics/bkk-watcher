<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>
{
	 "id": "nearest_entities",
	 "items": [
		<c:forEach var="entity" items="${bkkEntities}" varStatus="loop">
			   {
			   	  "type": "${entity.type}",
			      "route_id": "${entity.route_id}",
			      "route": "${entity.route}",
			      "stop_id": "${entity.stop_id}",
			      "stop": "${entity.stop}",
			      "longitude": ${entity.longitude},
			      "latitude": ${entity.latitude}
			   }${!loop.last ? ',' : ''}
		</c:forEach>
] }
