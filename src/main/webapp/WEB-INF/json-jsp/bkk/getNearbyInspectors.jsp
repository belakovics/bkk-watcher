<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>
{
     "id": "alerts",
     "items": [
		<c:forEach var="entity" items="${alerts}" varStatus="loop">
			   {
			   	  "route_id": "${entity.route_id}",
			      "route": "${entity.route}",
			      "stop_id": "${entity.stop_id}",
			      "stop": "${entity.stop}",
			      "type": "${entity.type}",
			      "longitude": ${entity.longitude},
			      "latitude": ${entity.latitude},
			      "frequency": ${entity.frequency},
			      "lastreport": "${entity.timesp}"
			   }${!loop.last ? ',' : ''}
		</c:forEach>
	]
}
