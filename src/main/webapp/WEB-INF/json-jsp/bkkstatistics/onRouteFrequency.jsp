<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>
{ "id": "nearest_entities", "items": [
<c:forEach var="entity" items="${bkkEntities}" varStatus="loop">
			   {
			      "route_id": "${entity.route_id}",
			      "route": "${entity.route}",
			      "frequency": "${entity.frequency}",
			   }${!loop.last ? ',' : ''}
		</c:forEach>
] }<%-- TODO SOLVE: bkkStaistics response minta kialakitasa : route routeid name frequency
GOOGLE: iterate on hashmap bkkEntities helyett...--> 
