<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>
{
	 "id": "predictions",
	 "items": [
		<c:forEach var="entity" items="${predictions}" varStatus="loop">
			   {
			   	  "type": "${entity.type}",
			      "route_id": "${entity.route_id}",
			      "route": "${entity.route}",
			      "stop_id": "${entity.stop_id}",
			      "stop": "${entity.stop}",
			      "longitude": ${entity.longitude},
			      "latitude": ${entity.latitude},
			      "frequency": ${entity.frequency},
			      "maxfrequency": ${entity.maxfrequency}
			   }${!loop.last ? ',' : ''}
		</c:forEach>
] }
