<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>
{
	"id": "dangerousstops",
	"maxfrequency": ${maxfrequency},
	"items": [
<c:forEach var="stop" items="${stops}" varStatus="loop">
			   {
			   	  "stop_id": "${stop.stop_id}",
			   	  "stop": "${stop.stop}",
			   	  "route_id": "${stop.route_id}",
			   	  "route": "${stop.route}",
			   	  "type": "${stop.type}",
			   	  "latitude": ${stop.latitude},
			   	  "longitude": ${stop.longitude},
			   	  "frequency": ${stop.frequency}
			   }${!loop.last ? ',' : ''}
</c:forEach>
	]
}
