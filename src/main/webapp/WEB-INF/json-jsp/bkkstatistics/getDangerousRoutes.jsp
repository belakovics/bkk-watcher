<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>
{
	"id": "dangerousroutes",
	"maxfrequency": ${maxfrequency},
	"items": [
<c:forEach var="routeItem" items="${routes}" varStatus="loopRoute">
			   {
			   		"route_id": "${routeItem.key.route_id}",
			   	  	"route": "${routeItem.key.route}",
			   	  	"type": "${routeItem.key.type}",
			   	  	"frequency": ${routeItem.key.frequency},
			   	  	"stops": [
					<c:forEach var="stopItem" items="${routeItem.value}" varStatus="loopStop">
			   				{
			   					"stop": "${stopItem.stop}",
			   					"latitude": ${stopItem.latitude},
			   					"longitude": ${stopItem.longitude}
			   				}${!loopStop.last ? ',' : ''}
					</c:forEach>
					]								   	  
			   }${!loopRoute.last ? ',' : ''}
</c:forEach>
	]
}
