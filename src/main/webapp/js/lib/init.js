$(function() {
	
	//console.log('Fingerprint: ' + $.fingerprint());
	
	if (navigator.geolocation) {
		console.log('Device has geolocation support');
	} else {
		// geolocation not available
		Modal.showStatusDialog("Sorry, your browser doesn't support geolocation.");
		console.log('Device has no geolocation support');
		
		return;
	}

	craeteBindings();
	console.log('Event bindings done');

	var rtime = new Date(1, 1, 2000, 12, 00, 00);
	var timeout = false;
	var delta = 200;
	var activeMap;

	if ($('#page-report').length > 0) {
		activeMap = 'report-map-canvas';
		console.log('Report page is active');
	}
	else if ($('#page-overview').length > 0) {
		activeMap = 'overview-map-canvas';
		console.log('Overview page is active');
	}

	$(window).resize(function() {
		rtime = new Date();
		if (timeout === false) {
			timeout = true;
			setTimeout(resizeEnd, delta);
		}
	});

	function resizeEnd() {
		if (new Date() - rtime < delta) {
			setTimeout(resizeEnd, delta);
		} else {
			timeout = false;
			if (activeMap !== undefined) {
				GeoLocation.mapRefresh();
				GeoLocation.refresh();
				console.log('Window resized');
			}
		}
	}
	;
});

craeteBindings = function() {

	var timer;

	$('#btnReport').on('click', function() {
				Service.showNearbyStops(GeoLocation.currentLocation.longitude,
						GeoLocation.currentLocation.latitude);
			});

	$('#stopInputDialog').on('click', '.btnStop', function() {
		Service.report($(this).attr('route-id'), $(this).attr('stop-id'));
	});
	
	console.log('Report button binding done');

	$('input[type="checkbox"]').on('click', function() {

		if (this.checked === true) {

			alertTimer();

			timer = setInterval(alertTimer, 10 /* sec */* 1000);
		} else {
			Service.clear();
			clearInterval(timer);
		}
	});

	console.log('Checkbox binding done');
	
	$('#overviewType').on('click', function() {
	    $('#overviewType').blur();
	});
	
	$('#overviewType').on('change', function() {
		
				if (this.value === 'dangerousRoutes') {
					Service.clear();
					Service.showDangerousRoutes(GeoLocation.getMap('overview-map-canvas'));
				} else if (this.value === 'dangerousStops') {
					Service.clear();
					Service.showDangerousStops(GeoLocation.getMap('overview-map-canvas'));
				} else if (this.value === 'forecastToday') {
					Service.clear();
					Service.showForecastToday(GeoLocation.getMap('overview-map-canvas'));
				}
			});
	
	console.log('Combobox binding done');
};

alertTimer = function() {

	if ($('#page-report').length > 0) {

		console.log('Refresh live alerts');
		
		var map = GeoLocation.getMap('report-map-canvas');

		if (map !== undefined) {
			Service.showAlerts(map, GeoLocation.currentLocation.longitude,
					GeoLocation.currentLocation.latitude);
		}
	}
};

google.maps.event.addDomListener(window, 'load', function() {

	if (navigator.geolocation) {
		GeoLocation.init('report-map-canvas', 500, {
			zoom : 14,
			mapTypeId : google.maps.MapTypeId.ROADMAP
		});
		GeoLocation.init('overview-map-canvas', undefined, {
			zoom : 13,
			mapTypeId : google.maps.MapTypeId.ROADMAP
		});
		
		console.log('Google Maps are initialized');
	}
});
