
/**
 * Modal dialog class. Responsible for the show status and error messages
 * @namespace BKKWatcher
 * @alias Modal
 */
var Modal = function() {

  /**
   * Open a loading dialog with spinning loader. If the message empty then only
   * the spinning loader sign will be shown without dialog. If the timeout is 
   * defined then a close button will be shown on the dialog.
   * @param {string} message The message will be shown in the dialog. The message can contain html.
   */
   showLoadingDialog = function(message) {
      $.mobile.loading('show', {
         text: message !== undefined ? message : '',
         textVisible: message !== undefined ? true : false,
         theme: 'b',
         textonly: false,
         html: ''
      });
   };

   /**
   * Open an error dialog. If the timeout is defined then a close button will be shown on the dialog.
   * @param {string} message The message will be shown in the dialog. The message can contain html.
   * @param {number} timeout Optional. After the timeout (ms) the dialog will be hide automatically.
   */
   showErrorDialog = function(message, timeout) {

      var html = '' +
              '<h3>Error occured</h3>' +
              '<div class="error-box">' +
              '	<p>Description:</p>' +
              '	<p>' + message + '</p>';

      if (timeout === undefined)
         html += '	<a href="#" onClick="closeDialog();" data-theme="f" class="ui-btn ui-shadow ui-btn-corner-all ui-btn-up-f centered" style="max-width: 70em;">Close</a>';

      html += '</div>' +
              '';

      if (message !== undefined)
      {
         $.mobile.loading('show', {
            text: '',
            textVisible: true,
            theme: 'f',
            textonly: true,
            html: html
         });

         if (timeout !== undefined) {
            setTimeout(function() {
               closeDialog();
            }, timeout);
         }
      }
      else
         console.log('Error: missing message text value');
   };
   
   /**
   * Open a extended status dialog. If the timeout is defined then a close button will be shown on the dialog.
   * @param {string} message The message will be shown in the dialog. The message can contain html.
   * @param {number} timeout Optional. After the timeout (ms) the dialog will be hide automatically.
   * @param {string} header Optional. The title of the dialog.
   */
   showStatusDialogEx = function(message, timeout, header) {

      var html = '';

      if (header !== undefined)
         html += '<h3>' + header + '</h3>';

      html += '<div class="status-box">' +
              '	<p>' + message + '</p>';

      if (timeout === undefined)
         html += '	<a href="#" onClick="closeDialog();" data-theme="b" class="ui-btn ui-shadow ui-btn-corner-all ui-btn-up-b centered" style="max-width: 70em;">Close</a>';

      html += '</div>' +
              '';

      if (message !== undefined)
      {
         $.mobile.loading('show', {
            text: '',
            textVisible: true,
            theme: 'b',
            textonly: true,
            html: html
         });

         if (timeout !== undefined) {
            setTimeout(function() {
               closeDialog();
            }, timeout);
         }
      }
      else
         console.log('Error: missing message text value');
   };

   /**
   * Open a status dialog. If the timeout is defined then a close button will be shown on the dialog.
   * @param {string} message The message will be shown in the dialog. The message can contain html.
   * @param {number} timeout Optional. After the timeout (ms) the dialog will be hide automatically.
   */
   showStatusDialog = function(message, timeout) {

      var html = '' +
              '<div class="status-box" style="text-align: center;">' +
              '	<p>' + message + '</p>';

      if (timeout === undefined)
         html += '	<a href="#" onClick="closeDialog();" data-theme="b" class="ui-btn ui-shadow ui-btn-corner-all ui-btn-up-b centered" style="max-width: 70em;">Close</a>';

      html += '</div>' +
              '';

      if (message !== undefined)
      {
         $.mobile.loading('show', {
            text: '',
            textVisible: true,
            theme: 'b',
            textonly: true,
            html: html
         });

         if (timeout !== undefined) {
            setTimeout(function() {
               closeDialog();
            }, timeout);
         }
      }
      else
         console.log('Error: missing message text value');
   };

   /**
   * Close the last shown dialog.
   */
   closeDialog = function() {
      $.mobile.loading('hide');
   };

   return {
      showLoadingDialog: showLoadingDialog,
      showErrorDialog: showErrorDialog,
      showStatusDialogEx: showStatusDialogEx,
      showStatusDialog: showStatusDialog,
      closeDialog: closeDialog
   };
}();

/*
 $.ajax({
 type: "POST",
 cache: false,
 contentType: "application/json; charset=utf-8",
 dataType: "json",
 url: "",
 data: "",
 success: function (r) {
 
 }, 
 beforeSend: function (XMLHttpRequest) {
 $.mobile.showPageLoadingMsg();
 },  
 complete: function (XMLHttpRequest, textStatus) {
 $.mobile.hidePageLoadingMsg();
 },
 error: function (xmlHttpRequest, status, err) {
 $.mobile.hidePageLoadingMsg();
 }
 */