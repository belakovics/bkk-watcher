GeoLocation = function() {

	var maps = [];
	var currentLocation = {
		longitude : 0,
		latitude : 0,
		accuracy : 0
	};

	init = function(aMapId, range, options) {

		if (aMapId !== undefined) {

			navigator.geolocation.getCurrentPosition(function(pos) {

				currentLocation.longitude = pos.coords.longitude;
				currentLocation.latitude = pos.coords.latitude;
				currentLocation.accuracy = pos.coords.accuracy;

				// we need only one map options currently, init with Budapest
				//            var latlng = new google.maps.LatLng(47.498053, 19.040658);
				var latlng = new google.maps.LatLng(pos.coords.latitude,
						pos.coords.longitude);

				var mapOptions= options;

				if (options === undefined) {
					mapOtions = {
						zoom : 14,
						center : latlng,
						mapTypeId : google.maps.MapTypeId.ROADMAP
					};
				} else {
					mapOptions.center= latlng;
				}

				var container = document.getElementById(aMapId);

				if (container !== null) {

					var map = new google.maps.Map(container, mapOptions);
					var image = {
						url : 'images/blue_dot.png',
						size : new google.maps.Size(20, 20),
						origin : new google.maps.Point(0, 0),
						anchor : new google.maps.Point(10, 10)
					};
					var marker = new google.maps.Marker({
						position : latlng,
						map : map,
						icon : image
					});

					maps.push({
						id : aMapId,
						map : map,
						marker : marker
					});

					if (range !== undefined) {
						var circle = new google.maps.Circle({
							map : map,
							radius : range, // metres
							fillColor : '#AA0000'
						});

						circle.bindTo('center', marker, 'position');
					}

					google.maps.event.trigger(map, 'resize');
				}
			});
		}

		try {
			watchID = navigator.geolocation.watchPosition(success, error, {
				enableHighAccuracy : true
			});
		} catch (err) {
			Modal.showErrorDialog(err);
			console.log(err);
		}

	};

	getDetailsHtml = function() {
		var str = '';

		if (currentLocation !== undefined) {
			if (currentLocation.longitude !== undefined)
				str += '<span class="black"> Lng: ' + currentLocation.longitude
						+ ' </span>';
			if (currentLocation.latitude !== undefined)
				str += '<span class="black"> Lat: ' + currentLocation.latitude
						+ ' </span>';
			if (currentLocation.accuracy !== undefined)
				str += '<span class="black"> Acc: ' + currentLocation.accuracy
						+ ' </span>';
		}
		str = '<p>' + str + '</p>';

		return str;
	};

	mapRefresh = function() {
		$.each(maps, function() {
				google.maps.event.trigger(this.map, 'resize');
		});
	};

	refresh = function() {
		if (currentLocation.longitude !== 0 && currentLocation.latitude !== 0) {

			$('#lng').html(currentLocation.longitude.toFixed(6));
			$('#lat').html(currentLocation.latitude.toFixed(6));

			var point = new google.maps.LatLng(currentLocation.latitude,
					currentLocation.longitude);

			$.each(maps, function() {
				if (this.marker !== undefined) {
					this.marker.setPosition(point);
					this.map.setCenter(point);
				}
			});
		}
	};

	success = function(pos) {

		currentLocation.longitude = pos.coords.longitude;
		currentLocation.latitude = pos.coords.latitude;
		currentLocation.accuracy = pos.coords.accuracy;

		refresh();
	};

	error = function() {
		output.innerHTML = "Unable to retrieve your location";
	};

	getMap = function(id) {
		var res;

		$.each(maps, function() {
			if (this.id === id)
				res = this.map;
		});

		return res;
	};

	return {
		init : init,
		refresh : refresh,
		mapRefresh : mapRefresh,
		getMap : getMap,
		currentLocation : currentLocation
	};
}();