/**
 * Service class. Responsible for the service layer of the webclient. It
 * communicates with the backend via ajax REST service calls.
 * 
 * @namespace
 * @alias Service
 */
var Service = function() {

	var OFFLINE = true;
	var FILEROOT = '/services.json/';

	var routesArray = [];
	var markersArray = [];
	var alertsArray = [];

	var liveAlertsArray = [];
	var lastAlerts = undefined;

	var SERVICES = {
		getNearbyStops : 'bkkwatcher/bkk/getNearbyStops',
		getNearbyInspectors : 'bkkwatcher/bkk/getNearbyInspectors',
		report : 'bkkwatcher/bkk/report',
		// BKK related statistic services
		getDangerousStops : 'bkkwatcher/bkkstatistics/getDangerousStops',
		getDangerousRoutes : 'bkkwatcher/bkkstatistics/getDangerousRoutes',
		// Forecast
		getPrediction: 'bkkwatcher/bkkstatistics/getPrediction'
	};

	/**
	 * Report a ticket inspector to the nearby stop of the GPS coordinates. It
	 * can throw exception when error occure.
	 * 
	 * @param {double}
	 *            lng The longitude.
	 * @param {double}
	 *            lat The latitude.
	 */
	showNearbyStops = function(lng, lat) {
		try {
			AJAX(SERVICES.getNearbyStops, {
				longitude : lng,
				latitude : lat
			},
			// case of success
			function(response) {
				if (response.items.length === 0) {
					Modal.showStatusDialog('No stops in your nearby.', 1500);
					return;
				}

				var $ul = $('<ul>');
				$ul.append($("#stopListItem").tmpl(response.items));

				$('#lStops').html($ul);
				$('#btnShowStops').click();

				$ul.attr('data-theme', 'c');
				$ul.attr('data-inset', true);
				$ul.listview();
			});
		} catch (ex) {
			Modal.showErrorDialog(ex.toString());
			console.log(ex.toString());
		}
	};

	report = function(routeId, stopId) {
		try {
			AJAX(SERVICES.report, {
				routeid : routeId,
				stopid : stopId
			},
			// case of success
			function(response) {
				if (response.error !== undefined) {
					Modal.showErrorDialog(response.error);
					return;
				}

				Modal.showStatusDialog('Report successfully stored at '
						+ response.timesp, 2500);

			}, false);

			$("#stopInputDialog").dialog("close");

		} catch (ex) {
			Modal.showErrorDialog(ex.toString());
			console.log(ex.toString());
		}
	};

	showAlerts = function(map, lng, lat) {
		if (map === undefined) {
			Modal.showErrorDialog('Missing map!');
			return;
		}

		try {
			AJAX(
					SERVICES.getNearbyInspectors,
					{
						longitude : lng,
						latitude : lat
					},
					// case of success
					function(response) {
						
						if (response === undefined) {
							Modal.showErrorDialog("No response from the server.");
							return;
						}
						
						if (response.error !== undefined) {
							Modal.showErrorDialog(response.error);
							return;
						}
						var newAlerts = response;
						
						// place to check diff between the response and the last response
						// and only show the new ones
						
						clear();
						
						$
								.each(
										newAlerts.items,
										function() {
											var latlng = new google.maps.LatLng(
													this.latitude,
													this.longitude);
											var image = {
												url : 'images/alert.png',
												size : new google.maps.Size(34,
														34),
												origin : new google.maps.Point(
														0, 0),
												anchor : new google.maps.Point(
														17, 17)
											};
											var marker = new google.maps.Marker(
													{
														position : latlng,
														map : map,
														icon : image
													});
											var infowindow = new google.maps.InfoWindow(
													{});
											liveAlertsArray.push(marker);

											google.maps.event
													.addListener(
															marker,
															'click',
															(function(marker,
																	item) {
																return function() {
																	infowindow
																			.setContent('<div style="color:black;">'
																					+ '<table><tr>'
																					+ '<td rowspan="2"><img style="width: 43px; height: 43px;" src="images/'
																					+ item.type
																					+ '.svg">'
																					+ '</td>'
																					+ '<td style="text-align:right;">'
																					+ item.route
																					+ '</td><td>'
																					+ item.stop
																					+ '</td></tr>'
																					+ '<tr><td colspan="2" style="font-size:12px;">Number of reports: '
																					+ item.frequency
																					+ '</td></tr></table>'
																					+ '</div>');
																	infowindow
																			.open(
																					map,
																					marker);
																};
															})(marker, this));
										});
					}, false, false);
		} catch (ex) {
			Modal.showErrorDialog(ex.toString());
			console.log(ex.toString());
		}
	};

	showDangerousRoutes = function(map) {

		if (map === undefined) {
			Modal.showErrorDialog('Missing map!');
			return;
		}

		try {
			AJAX(SERVICES.getDangerousRoutes, undefined,
			// case of success
			function(response) {
				if (response.error !== undefined) {
					Modal.showErrorDialog(response.error);
					return;
				}

				var maxFrequency = response.maxfrequency;

				$.each(response.items, function() {
					var route = [];

					$.each(this.stops, function() {
						route.push(new google.maps.LatLng(this.latitude,
								this.longitude));
					});
					var opacitiy = 1;

					if ((maxFrequency / 3) > this.frequency)
						opacitiy = 0.15;
					else if (((maxFrequency / 3) * 2 > this.frequency)
							&& (maxFrequency / 3) < this.frequency)
						opacitiy = 0.30;

					var routePath = new google.maps.Polyline({
						path : route,
						strokeColor : '#FF0000',
						strokeOpacity : Math.sqrt(opacitiy),
						strokeWeight : 4
					});

					routePath.setMap(map);

					routesArray.push(routePath);
				});
			});

		} catch (ex) {
			Modal.showErrorDialog(ex.toString());
			console.log(ex.toString());
		}
	};

	showDangerousStops = function(map) {

		if (map === undefined) {
			Modal.showErrorDialog('Missing map!');
			return;
		}

		try {
			AJAX(
					SERVICES.getDangerousStops,
					undefined,
					// case of success
					function(response) {
						if (response.error !== undefined) {
							Modal.showErrorDialog(response.error);
							return;
						}

						$
								.each(
										response.items,
										function() {
											var latlng = new google.maps.LatLng(
													this.latitude,
													this.longitude);
											var image = {
												url : 'images/alert.png',
												size : new google.maps.Size(34,
														34),
												origin : new google.maps.Point(
														0, 0),
												anchor : new google.maps.Point(
														17, 17)
											};
											var marker = new google.maps.Marker(
													{
														position : latlng,
														map : map,
														icon : image
													});
											var infowindow = new google.maps.InfoWindow(
													{});

											markersArray.push(marker);

											google.maps.event
													.addListener(
															marker,
															'click',
															(function(marker,
																	item) {
																return function() {
																	infowindow
																			.setContent('<div style="color:black;">'
																					+ '<table><tr>'
																					+ '<td rowspan="2"><img style="width: 43px; height: 43px;" src="images/'
																					+ item.type
																					+ '.svg">'
																					+ '</td>'
																					+ '<td style="text-align:right;">'
																					+ item.route
																					+ '</td><td>'
																					+ item.stop
																					+ '</td></tr>'
																					+ '<tr><td colspan="2" style="font-size:12px;">Number of reports: '
																					+ item.frequency
																					+ '</td></tr></table>'
																					+ '</div>');
																	infowindow
																			.open(
																					map,
																					marker);
																};
															})(marker, this));
										});
					});
		} catch (ex) {
			Modal.showErrorDialog(ex.toString());
			console.log(ex.toString());
		}
	};

	showForecastToday = function(map) {

		if (map === undefined) {
			Modal.showErrorDialog('Missing map!');
			return;
		}

		try {
			AJAX(
					SERVICES.getPrediction,
					undefined,
					// case of success
					function(response) {
						if (response.error !== undefined) {
							Modal.showErrorDialog(response.error);
							return;
						}

						$
								.each(
										response.items,
										function() {
											var latlng = new google.maps.LatLng(this.latitude, this.longitude);
											var image = {
												url : 'images/alert.png',
												size : new google.maps.Size(34, 34),
												origin : new google.maps.Point(0, 0),
												anchor : new google.maps.Point(17, 17)
											};
											var marker = new google.maps.Marker(
													{
														position : latlng,
														map : map,
														icon : image
													});
											var infowindow = new google.maps.InfoWindow(
													{});

											markersArray.push(marker);

											google.maps.event
													.addListener(
															marker,
															'click',
															(function(marker,
																	item) {
																return function() {
																	infowindow
																			.setContent('<div style="color:black;">'
																					+ '<table><tr>'
																					+ '<td rowspan="2"><img style="width: 43px; height: 43px;" src="images/'
																					+ item.type
																					+ '.svg">'
																					+ '</td>'
																					+ '<td style="text-align:right;">'
																					+ item.route
																					+ '</td><td>'
																					+ item.stop
																					+ '</td></tr>'
																					+ '<tr><td colspan="2" style="font-size:12px;">Chance to meet with inspector: '
																					+ ((item.frequency / item.maxfrequency) * 100).toFixed(2) + '%'
																					+ '</td></tr></table>'
																					+ '</div>');
																	infowindow
																			.open(
																					map,
																					marker);
																};
															})(marker, this));
										});
					});
		} catch (ex) {
			Modal.showErrorDialog(ex.toString());
			console.log(ex.toString());
		}
	};
	
	clear = function() {
		for ( var i = 0; i < markersArray.length; i++) {
			markersArray[i].setMap(null);
		}
		markersArray.length = 0;

		for ( var i = 0; i < routesArray.length; i++) {
			routesArray[i].setMap(null);
		}
		routesArray.length = 0;

		for ( var i = 0; i < alertsArray.length; i++) {
			alertsArray[i].setMap(null);
		}
		alertsArray.length = 0;

		for ( var i = 0; i < liveAlertsArray.length; i++) {
			liveAlertsArray[i].setMap(null);

			if (i == 0)
				lastAlerts = undefined;
		}
		liveAlertsArray.length = 0;
	};
	forecast = function() {
		throw $.error("Not implemented yet.");
	};

	AJAX = function(url, data, successHandler, showLoadingDialog, asyncAjax) {

		var file = FILEROOT + url + '.json';

		$.get(file, function(res) {
			successHandler(res);
		}, 'json').fail(
				function() {
					var async= true;
					
					if (asyncAjax !== undefined)
						if (asyncAjax === false)
							async = false;
					
					var showDialog = true;

					$.ajax({
						url : url,
						type : "post",
						data : data,
						async : async,
						beforeSend : function() {
							console.log('AJAX: ' + url + ' service called.');
							if (showLoadingDialog !== undefined)
								if (showLoadingDialog === false)
									showDialog = false;

							if (showDialog)
								Modal.showLoadingDialog();
						},
						success : function(resp) {
							successHandler(resp);

							if (showDialog)
								Modal.closeDialog();
						},
						error : function(XHR, textStatus, errorThrown) {
							Modal.showErrorDialog('Service ' + textStatus + ":"
									+ errorThrown);
							console.log(textStatus + ":" + errorThrown);
						}
					});
				});
	};

	jsondiff = function(firstJson, secondJson) {
		var result = {};
		
		for (key in firstJson) {
			if (secondJson[key] !== firstJson[key])
				result[key] = secondJson[key];
			if (typeof secondJson[key] === 'array'
					&& typeof firstJson[key] === 'array')
				result[key] = arguments.callee(firstJson[key], secondJson[key]);
			if (typeof secondJson[key] === 'object'
					&& typeof firstJson[key] === 'object')
				result[key] = arguments.callee(firstJson[key], secondJson[key]);
		}
		
		return result;
	};

	return {
		clear : clear,
		// Report services
		report : report,
		showNearbyStops : showNearbyStops,
		showAlerts : showAlerts,
		// Overview services
		showDangerousRoutes : showDangerousRoutes,
		showDangerousStops : showDangerousStops,
		// Forecast
		showForecastToday : showForecastToday
	};
}();
