package bkkwatcher.statistics.business;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

public class CalculateFrequency {
	
	/* sort the HashMap by it's value's frequency */
	public HashMap<Object, Integer> sortByFrequency(ArrayList<Object> list){
		HashMap<Object, Integer> unsortedMap = new HashMap<Object, Integer>();
		
		for(int i = 0; i < list.size(); i++){
			unsortedMap.put(list.get(i), getFrequency(list, list.get(i)));
		}
		
		HashMap<Object, Integer> sortedMap = sortByComparator(unsortedMap);
		
		return sortedMap;
	}
	
	/* Calculate the frequency of the o Object from the arrlist ArrayList */
	private int getFrequency(ArrayList<Object> arrlist, Object o){
		
		int freq = Collections.frequency(arrlist, o);
		
		return freq;
	}
	
	/* sort HashMap by Value */
	private HashMap<Object, Integer> sortByComparator(HashMap<Object, Integer> unsortMap) {
		 
		LinkedList<Map.Entry<Object, Integer>> list = new LinkedList<Entry<Object, Integer>>(unsortMap.entrySet());
 
		/* sort list based on comparator */
		Collections.sort(list, new Comparator<Map.Entry<Object, Integer>>() {
			public int compare(Map.Entry<Object, Integer> o1, Map.Entry<Object, Integer> o2) {
				return o2.getValue().compareTo(o1.getValue());
			}
		});
 
		/* put sorted list into map again */
        /* LinkedHashMap make sure order in which keys were inserted */
		HashMap<Object, Integer> sortedMap = new HashMap<Object, Integer>();
		for (Iterator<Map.Entry<Object, Integer>> it = list.iterator(); it.hasNext();) {
			Map.Entry<Object, Integer> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}
}
