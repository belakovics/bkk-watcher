package bkkwatcher.statistics.business.domain;

import java.util.List;
import org.svenson.JSONTypeHint;

public class StatisticsInput
{
    @SuppressWarnings("unused")
	private List<StatisticsInputItem> items;

    @JSONTypeHint(StatisticsInputItem.class)
    public void setItems(List<StatisticsInputItem> items)
    {
        this.items = items;
    }
}