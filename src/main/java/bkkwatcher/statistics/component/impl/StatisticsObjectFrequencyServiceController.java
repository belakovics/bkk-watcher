package bkkwatcher.statistics.component.impl;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import bkkwatcher.statistics.business.CalculateFrequency;

/**
 *
 * TASK (SUBLAYER): Tasks represent a small set of services that logically belong together
 * 					and enable the user to perform a certain task.
 *
 */
public class StatisticsObjectFrequencyServiceController extends MultiActionController
{
	
	/**
	 * For logging
	 */
	private Logger logger = LoggerFactory.getLogger(getClass());
	/**
	 * 
	 * @param request
	 * @param response
	 * @return
	 * 
	 * When you select a process or a task, the 'job' method is called.
	 * 
	 */
	public ModelAndView job( HttpServletRequest request, HttpServletResponse response )
	{
		logger.debug("StatisticsObjectFrequencyServiceController job called");
		
		ModelAndView mav = new ModelAndView();
		
		mav.addObject("errorMsg", "Service not implemented yet!");
		
		mav.setViewName("general/error");
		
		return mav;
	}
	
	public HashMap<Object, Integer> calculateFrequency(ArrayList<Object> list) {
		CalculateFrequency calc_freq = new CalculateFrequency();
		HashMap<Object, Integer> result = calc_freq.sortByFrequency(list);
		return result;
	}
}
