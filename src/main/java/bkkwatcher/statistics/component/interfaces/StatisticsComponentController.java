package bkkwatcher.statistics.component.interfaces;

import bkkwatcher.statistics.component.impl.StatisticsObjectFrequencyServiceController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

/**
 * 
 * SERVICE LAYER: 1st layer.
 * 				  The service layer offers the functions that are called from the client (browser).
 * 				  It calls methods of the business layer to get the required data and renders them in the view jsp files.
 * 
 * 				  The standard message format used between the client and the servers is
 * 				  JSON, every back-end system generates it's response in this format.
 * 
 * PROCESS (SUBLAYER): Processes are the larger set of services, that you can normally access from the application menu.
 * 					   Generally they contain one or more tasks that appear on tabs in the content pane.
 *
 */
public class StatisticsComponentController extends MultiActionController
{
	
	protected StatisticsObjectFrequencyServiceController statisticsObjectFrequencyServiceController;

	/**
	 * 
	 * @param setStatisticsService1Controller
	 * 
	 * Beans are instantiated by the Web Container based on the spring XML configuration files.
	 * The same applies for the class members, they are set using the setter methods, hence the Required annotation.
	 * This dependency injection provides high flexibility.
	 * 
	 */
	@Required
	public void setStatisticsObjectFrequencyServiceController(StatisticsObjectFrequencyServiceController statisticsObjectFrequencyServiceController)
	{
		this.statisticsObjectFrequencyServiceController = statisticsObjectFrequencyServiceController;
	}
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @return
	 * 
	 * When you select a process or a task, the 'init' method is called.
	 * In case of a process, this will normally call an 'init' of its first task.
	 * 
	 */
	public ModelAndView service1( HttpServletRequest request, HttpServletResponse response )
	{
		logger.debug("service1");
		
		return statisticsObjectFrequencyServiceController.job( request, response );
	}	
}
