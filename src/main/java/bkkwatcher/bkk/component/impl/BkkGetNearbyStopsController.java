package bkkwatcher.bkk.component.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import bkkwatcher.persistence.BkkDao;
import bkkwatcher.persistence.entity.BkkEntity;

/**
 *
 * TASK (SUBLAYER): Tasks represent a small set of services that logically belong together
 * 					and enable the user to perform a certain task.
 *
 */
public class BkkGetNearbyStopsController extends MultiActionController
{
	@Required
	public void setBkkDao(BkkDao bkkDao)
	{
		this.bkkDao = bkkDao;
	}

	private Logger logger = LoggerFactory.getLogger(getClass());
	protected BkkDao bkkDao;
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @return
	 * 
	 * When you select a process or a task, the 'job' method is called.
	 * 
	 */
	public ModelAndView job( HttpServletRequest request, HttpServletResponse response )
	{
		logger.info("BkkGetNearbyStopsController job called");

		double longitudeF= 19.0255;
		double latitudeF= 47.51;
		
		try {
			String longitude = ((String[]) request.getParameterMap().get("longitude"))[0];
			String latitude = ((String[]) request.getParameterMap().get("latitude"))[0];
			
			longitudeF= Double.parseDouble(longitude);
			latitudeF= Double.parseDouble(latitude);
			
			logger.info("Incoming parameters longitude: " + longitudeF + ", latitude: " + latitudeF);
			
		} catch (Exception e) {
			logger.info("No input arguments");
		}
		
		List<BkkEntity> res = getCloseBkkEntities(longitudeF, latitudeF);
		
		// Spring MVC object
		ModelAndView mav = new ModelAndView();
		
		mav.addObject("bkkEntities", res);
		
		mav.setViewName("bkk/getNearbyStops");
		
		return mav;
	}

	public List<BkkEntity> getCloseBkkEntities(double longitudeF, double latitudeF) {
		List<BkkEntity> res= bkkDao.getNearbyBkkEntity(longitudeF, latitudeF, 500.0);
		return res;
	}
}
