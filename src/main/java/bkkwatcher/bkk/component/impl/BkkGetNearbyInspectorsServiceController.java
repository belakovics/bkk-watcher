package bkkwatcher.bkk.component.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import bkkwatcher.persistence.BkkDao;
import bkkwatcher.persistence.entity.BkkEntity;

public class BkkGetNearbyInspectorsServiceController extends MultiActionController
{	
	/**
	 * @param request (list of routes)
	 * @param response (list of reported inspector reports on routes in the last half hour)
	 * @return
	 */
	public ModelAndView job( HttpServletRequest request, HttpServletResponse response )
	{
		logger.debug("BkkGetNearbyInspectorsServiceController job called");
				
		double longitudeF= 0.0;
		double latitudeF= 0.0;
		
		try {
			String longitude = ((String[]) request.getParameterMap().get("longitude"))[0];
			String latitude = ((String[]) request.getParameterMap().get("latitude"))[0];
			
			longitudeF= Double.parseDouble(longitude);
			latitudeF= Double.parseDouble(latitude);
			
			logger.info("Incoming parameters longitude: " + longitudeF + ", latitude: " + latitudeF);
			
		} catch (Exception e) {
			logger.info("No input arguments");
		}
 
		List<BkkEntity> closeReportedBkkEnities= bkkDao.getNearbyReportedBkkEntity(longitudeF, latitudeF, 500, ONE_HOUR + LOCAL_TIME_DIFF);
		
		// Spring MVC object
		ModelAndView mav = new ModelAndView();
		
		mav.addObject("alerts", closeReportedBkkEnities);
		
		mav.setViewName("bkk/getNearbyInspectors");
		
		return mav;
	}
	
	@Required
	public void setBkkDao(BkkDao bkkDao) {
		this.bkkDao = bkkDao;
	}
	
	protected BkkDao bkkDao;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	private final static int ONE_HOUR = 1;
	private final static int LOCAL_TIME_DIFF = 23;
}
