package bkkwatcher.bkk.component.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import bkkwatcher.persistence.ReportDao;
import bkkwatcher.persistence.entity.ReportEntity;

/**
 * TASK (SUBLAYER): Conveys inspector report request with 
 * route and stop information to DB layer for further storage
 */
public class BkkReportInspectorServiceController extends MultiActionController
{
	/**
	 * @param request (route, stop)
	 * @param response (success)
	 * @return
	 */
	public ModelAndView job( HttpServletRequest request, HttpServletResponse response )
	{
		logger.debug("BkkReportInspectorServiceController job called");
		ReportEntity res = null;
		
		int routeId= 0;
		int stopId= 0;
		
		try {
			String route = ((String[]) request.getParameterMap().get("routeid"))[0];
			String stop = ((String[]) request.getParameterMap().get("stopid"))[0];
			
			routeId= Integer.parseInt(route);
			stopId= Integer.parseInt(stop);
		} catch (Exception e) {
			logger.info("No input arguments");
			
			ModelAndView mav = new ModelAndView();
			mav.addObject("errorMsg", "Missing input argument!");
			mav.setViewName("general/error");
			
			return mav;
		}
		
		//insert dummy line for test sake
		res = reportDao.reportInspector(routeId, stopId);
		
		
		// Spring MVC object
		ModelAndView mav = new ModelAndView();
		
		mav.addObject("report", res);
		
		mav.setViewName("bkk/report");
		
		return mav;
	}

	@Required
	public void setReportDao(ReportDao reportDao) {
		this.reportDao = reportDao;
	}
	
	protected ReportDao reportDao;
	private Logger logger = LoggerFactory.getLogger(getClass());
}
