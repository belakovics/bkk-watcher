package bkkwatcher.bkk.component.interfaces;

import bkkwatcher.bkk.component.impl.BkkGetNearbyInspectorsServiceController;
import bkkwatcher.bkk.component.impl.BkkGetNearbyStopsController;
import bkkwatcher.bkk.component.impl.BkkReportInspectorServiceController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

/**
 * 
 * SERVICE LAYER: 1st layer.
 * 				  The service layer offers the functions that are called from the client (browser).
 * 				  It calls methods of the business layer to get the required data and renders them in the view jsp files.
 * 
 * 				  The standard message format used between the client and the servers is
 * 				  JSON, every back-end system generates it's response in this format.
 * 
 * PROCESS (SUBLAYER): Processes are the larger set of services, that you can normally access from the application menu.
 * 					   Generally they contain one or more tasks that appear on tabs in the content pane.
 *
 */
public class BkkComponentController extends MultiActionController
{
	
	protected BkkGetNearbyStopsController bkkGetNearbyStopsController;
	protected BkkGetNearbyInspectorsServiceController bkkGetNearbyInspectorsServiceController;
	protected BkkReportInspectorServiceController bkkReportInspectorServiceController;

	/**
	 * 
	 * @param setBkkService1Controller
	 * 
	 * Beans are instantiated by the Web Container based on the spring XML configuration files.
	 * The same applies for the class members, they are set using the setter methods, hence the Required annotation.
	 * This dependency injection provides high flexibility.
	 * 
	 */
	@Required
	public void setBkkGetNearbyStopsController(BkkGetNearbyStopsController bkkGetNearbyStopsController)
	{
		this.bkkGetNearbyStopsController = bkkGetNearbyStopsController;
	}
	@Required
	public void setBkkGetNearbyInspectorsServiceController(BkkGetNearbyInspectorsServiceController bkkGetNearbyInspectorsServiceController)
	{
		this.bkkGetNearbyInspectorsServiceController = bkkGetNearbyInspectorsServiceController;
	}
	@Required
	public void setBkkReportInspectorServiceController(BkkReportInspectorServiceController bkkReportInspectorServiceController)
	{
		this.bkkReportInspectorServiceController = bkkReportInspectorServiceController;
	}
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @return
	 * 
	 * When you select a process or a task, the 'init' method is called.
	 * In case of a process, this will normally call an 'init' of its first task.
	 * 
	 */
	public ModelAndView getNearbyStops( HttpServletRequest request, HttpServletResponse response )
	{
		logger.info("Called service getNearbyStops");
		
		return bkkGetNearbyStopsController.job( request, response );
	}

	public ModelAndView getNearbyInspectors( HttpServletRequest request, HttpServletResponse response )
	{
		logger.debug("getNearbyInspectors");
		
		return bkkGetNearbyInspectorsServiceController.job( request, response );
	}
	
	public ModelAndView report( HttpServletRequest request, HttpServletResponse response )
	{
		logger.debug("report");
		
		return bkkReportInspectorServiceController.job( request, response );
	}
	
}
