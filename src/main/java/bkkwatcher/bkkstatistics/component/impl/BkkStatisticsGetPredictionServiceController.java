package bkkwatcher.bkkstatistics.component.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import bkkwatcher.persistence.BkkDao;
import bkkwatcher.persistence.entity.BkkEntity;

public class BkkStatisticsGetPredictionServiceController extends
		MultiActionController {
	protected BkkDao bkkDao;

	@Required
	public void setBkkDao (BkkDao bkkDao) {
		this.bkkDao = bkkDao;
	}

	/**
	 * 
	 * @param request (empty)
	 * @param response  ----
	 * @return (BkkEntity list)

	 */
	public ModelAndView job(HttpServletRequest request,
			HttpServletResponse response) {
		logger.debug("BkkStatisticsGetPredictionServiceController job called");

		// Spring MVC object
		ModelAndView mav = new ModelAndView();

		List<BkkEntity> result = bkkDao.getPredictionForDayOfWeek(); 
				
		mav.addObject("predictions", result);

		mav.setViewName("bkkstatistics/getPrediction");

		return mav;
	}
	
	private Logger logger = LoggerFactory.getLogger(getClass());
}
