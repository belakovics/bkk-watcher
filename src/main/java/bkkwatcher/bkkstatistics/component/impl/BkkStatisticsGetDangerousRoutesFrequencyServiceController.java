package bkkwatcher.bkkstatistics.component.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import bkkwatcher.persistence.BkkDao;
import bkkwatcher.persistence.ReportDao;
import bkkwatcher.persistence.entity.BkkEntity;
import bkkwatcher.persistence.entity.ReportEntity;
import bkkwatcher.statistics.component.impl.StatisticsObjectFrequencyServiceController;

/**
 * 
 *         TASK (SUBLAYER): Tasks represent a small set of services that
 *         logically belong together and enable the user to perform a certain
 *         task.
 * 
 */
public class BkkStatisticsGetDangerousRoutesFrequencyServiceController extends
		MultiActionController {
	protected ReportDao reportDao;

	@Required
	public void setReportDao(ReportDao reportDao) {
		this.reportDao = reportDao;
	}

	protected BkkDao bkkDao;

	@Required
	public void setBkkDao(BkkDao BkkDao) {
		this.bkkDao = BkkDao;
	}

	/**
	 * For logging
	 */
	private Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 
	 * @param request
	 * @param response
	 * @return
	 * 
	 *         When you select a process or a task, the 'job' method is called.
	 * 
	 */
	public ModelAndView job(HttpServletRequest request,
			HttpServletResponse response) {
		logger.debug("BkkStatisticsGetDangerousRoutesFrequencyServiceController job called");

		ArrayList<Object> reportsOnRoute = new ArrayList<Object>();

		for (ReportEntity report : reportDao.getAllReportEntity()) {
			reportsOnRoute.add(report.getRoute());
		}

		StatisticsObjectFrequencyServiceController statistics = new StatisticsObjectFrequencyServiceController();
		HashMap<Object, Integer> routeList = statistics.calculateFrequency(reportsOnRoute);
		
		int maxFrequency= 0;
		HashMap<BkkEntity, List<BkkEntity>> result= new HashMap<BkkEntity, List<BkkEntity>>();
		// populate hashmap with the list of corresponding stops
		for (Map.Entry<Object, Integer> entry : routeList.entrySet()) {
		    int route_id = (Integer) entry.getKey();
		    // get list of stops
		    List<BkkEntity> stopList= bkkDao.getBkkEntityByRoute(route_id);
		    
		    if(0 < stopList.size()) {
		    	
		    	if(maxFrequency < entry.getValue()) {
		    		maxFrequency = entry.getValue();
		    	}
		    	
		    	BkkEntity route= stopList.get(0);
		    	route.setFrequency(BigInteger.valueOf(entry.getValue()));
		    	
		    	result.put(route, stopList);
		    }
		}

		// Spring MVC object
		ModelAndView mav = new ModelAndView();

		mav.addObject("maxfrequency", maxFrequency);
		mav.addObject("routes", result);

		mav.setViewName("bkkstatistics/getDangerousRoutes");

		return mav;
	}
}
