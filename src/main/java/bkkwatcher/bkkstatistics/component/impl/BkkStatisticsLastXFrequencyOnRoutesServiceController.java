package bkkwatcher.bkkstatistics.component.impl;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import bkkwatcher.persistence.ReportDao;
import bkkwatcher.statistics.component.impl.StatisticsObjectFrequencyServiceController;

/**
 *
 * TASK (SUBLAYER): Tasks represent a small set of services that logically belong together
 * 					and enable the user to perform a certain task.
 *
 */
public class BkkStatisticsLastXFrequencyOnRoutesServiceController extends MultiActionController
{
	protected ReportDao reportDao;
	
	@Required
	public void setReportDao(ReportDao reportDao){
		this.reportDao = reportDao;
	}
	
	/**
	 * For logging
	 */
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @return
	 * 
	 * When you select a process or a task, the 'job' method is called.
	 * 
	 */
	public ModelAndView job( HttpServletRequest request, HttpServletResponse response)
	{
		logger.debug("BkkStatisticsLastXFrequencyOnRoutesServiceController job called");
		
		ArrayList<Object> reportsOnRoute = new ArrayList<Object>(reportDao.getLastXReportEntity(100));
		
		StatisticsObjectFrequencyServiceController statistics = new StatisticsObjectFrequencyServiceController();
		HashMap<Object, Integer> result = statistics.calculateFrequency(reportsOnRoute);
		
		// Spring MVC object
		ModelAndView mav = new ModelAndView();
		
		mav.addObject("OnRouteFrequencyLastX", result);
		
		mav.setViewName("bkkStatistics/OnRouteFrequencyLastX");
		
		return mav;
	}
}
