package bkkwatcher.bkkstatistics.component.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import bkkwatcher.persistence.BkkDao;
import bkkwatcher.persistence.ReportDao;
import bkkwatcher.persistence.entity.BkkEntity;
import bkkwatcher.persistence.entity.ReportEntity;
import bkkwatcher.statistics.component.impl.StatisticsObjectFrequencyServiceController;

/**
 *
 * TASK (SUBLAYER): Tasks represent a small set of services that logically belong together
 * 					and enable the user to perform a certain task.
 *
 */
public class BkkStatisticsGetDangerousStopsFrequencyServiceController extends MultiActionController
{
	protected ReportDao reportDao;
	
	@Required
	public void setReportDao(ReportDao reportDao){
		this.reportDao = reportDao;
	}

	protected BkkDao bkkDao;

	@Required
	public void setBkkDao(BkkDao BkkDao) {
		this.bkkDao = BkkDao;
	}

	/**
	 * For logging
	 */
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @return
	 * 
	 * When you select a process or a task, the 'job' method is called.
	 * 
	 */
	public ModelAndView job( HttpServletRequest request, HttpServletResponse response )
	{
		logger.debug("BkkStatisticsGetDangerousStopsFrequencyServiceController job called");
		
		ArrayList<Object> reportsOnRoute = new ArrayList<Object>();

		for (ReportEntity report : reportDao.getAllReportEntity()) {
			reportsOnRoute.add(report.getStop());
		}
		
		StatisticsObjectFrequencyServiceController statistics = new StatisticsObjectFrequencyServiceController();
		HashMap<Object, Integer> stopList = statistics.calculateFrequency(reportsOnRoute);
		
		int maxFrequency= 0;
		
		LinkedList<BkkEntity> result= new LinkedList<BkkEntity>();
		// populate hashmap with the list of corresponding stops
		for (Map.Entry<Object, Integer> entry : stopList.entrySet()) {
		    int stop_id = (Integer) entry.getKey();
		    
		    logger.info("Collect stop information for '" + stop_id + "' frequency '"+ entry.getValue() +"'.");
		    
		    // get list of stops
		    List<BkkEntity> stops= bkkDao.getBkkEntityByStop(stop_id);
		    
		    if(0 < stops.size()) {
		    	
		    	if(maxFrequency < entry.getValue()) {
		    		maxFrequency = entry.getValue();
		    	}
		    	
		    	BkkEntity stop= stops.get(0);
		    	stop.setFrequency(BigInteger.valueOf(entry.getValue()));
		    	
		    	result.add(stop);
		    }
		}
		
		// Spring MVC object
		ModelAndView mav = new ModelAndView();
		
		mav.addObject("maxfrequency", maxFrequency);
		mav.addObject("stops", result);
		
		mav.setViewName("bkkstatistics/getDangerousStops");
		
		return mav;
	}
}
