package bkkwatcher.persistence.entity;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

/**
 * 
 *         ENTITY (SUBLAYER): Here comes the entity classes.
 * 
 *         This would normally be a Hibernate annotated entity class for
 *         database records. To keep things simple, we will just mock the DB
 *         with 'wired' values :).
 * 
 */
@Entity
@Table(name = "bkk_v")
public class BkkEntity {

	// Entity definition
	private int id;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name = "id", nullable = false)
	public int getId() {
		return id;
	}

	public void setId(final int id) {
		this.id = id;
	}

	@Column(name = "type")
	private String type;

	@Column(name = "route_id")
	private Integer route_id;

	@Column(name = "route")
	private String route;

	@Column(name = "stop_id")
	private Integer stop_id;

	@Column(name = "stop")
	private String stop;

	@Column(name = "longitude")
	private float longitude;

	@Column(name = "latitude")
	private float latitude;

	@Transient
	private Date timesp;

	@Transient
	public Date getTimesp() {
		return timesp;
	}

	public void setTimesp(Date timesp) {
		this.timesp = timesp;
	}

	@Transient
	private BigInteger frequency;

	@Transient
	public BigInteger getFrequency() {
		return frequency;
	}

	public void setFrequency(BigInteger frequency) {
		this.frequency = frequency;
	}

	@Transient
	private BigInteger maxfrequency;

	@Transient
	public BigInteger getMaxfrequency() {
		return maxfrequency;
	}

	public void setMaxfrequency(BigInteger maxfrequency) {
		this.maxfrequency = maxfrequency;
	}

	public String getType() {
		return type;
	}

	// seters/getters
	public Integer getRoute_id() {
		return route_id;
	}

	public void setRoute_id(Integer route_id) {
		this.route_id = route_id;
	}

	public Integer getStop_id() {
		return stop_id;
	}

	public void setStop_id(Integer stop_id) {
		this.stop_id = stop_id;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public String getStop() {
		return stop;
	}

	public void setStop(String stop) {
		this.stop = stop;
	}

	public float getLongitude() {
		return longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	public float getLatitude() {
		return latitude;
	}

	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	@Override
	public String toString() {
		try {
			return "{ type: " + type + ", route_id: " + route_id + ", route: "
					+ route + ", stop_id: " + stop_id + ", stop: " + stop
					+ ", longitude: " + longitude + ", latitude: " + latitude
					+ ", timesp: " + timesp.toString() + ", frequency: "
					+ frequency.toString() + ", maxfrequency: "
					+ maxfrequency.toString() + " }";
		} catch (Exception e) {
			
		}
		
		return "";
	}

}
