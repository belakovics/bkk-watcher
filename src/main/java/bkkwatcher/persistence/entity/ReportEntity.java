package bkkwatcher.persistence.entity;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 *         ENTITY (SUBLAYER): Here comes the entity classes.
 * 
 *         This would normally be a Hibernate annotated entity class for
 *         database records. To keep things simple, we will just mock the DB
 *         with 'wired' values :).
 * 
 */
@Entity
@Table(name = "bkkreport")
public class ReportEntity {
	// Entity definition
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Integer id;

	@Column(name = "route")
	private Integer route;

	@Column(name = "stop")
	private Integer stop;

	@Column(name = "timesp", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date timesp;
	
	@PreUpdate
	public void autoTimeStamp() {
		java.util.Date date = new java.util.Date();
		this.timesp = new Timestamp(date.getTime());
	}
	

	public Integer getId() {
		return id;
	}

	// getters/setters
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRoute() {
		return route;
	}

	public void setRoute(Integer route) {
		this.route = route;
	}

	public Integer getStop() {
		return stop;
	}

	public void setStop(Integer stop) {
		this.stop = stop;
	}

	public Date getTimesp() {
		return timesp;
	}

	@Override
	public String toString() {
		return "{" + "  id: " + id + ", route: " + route + ", stop: " + stop	+ ", timesp: " + timesp.toString() + "}";
	}

}
