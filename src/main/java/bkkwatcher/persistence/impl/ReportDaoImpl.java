package bkkwatcher.persistence.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import bkkwatcher.persistence.ReportDao;
import bkkwatcher.persistence.entity.BkkEntity;
import bkkwatcher.persistence.entity.ReportEntity;

/**
 * 
 *         IMPL (SUBLAYER): Here come the implementing classes of the
 *         persistence interfaces.
 * 
 */
public class ReportDaoImpl implements ReportDao {

	/**
	 * For logging DAO events.
	 */
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Transactional
	public void save(ReportEntity report) {
		Session session = sessionFactory.getCurrentSession();
		session.save(report);
	}

	/**
	 * 
	 * A method like this would normally run a Hibernate query to fill the
	 * entity from the database. In this example we didn't want to use a
	 * database so we 'wired' the results inside this class to imitate the
	 * query.
	 * 
	 */
	
	protected SessionFactory sessionFactory;

	@Required
	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<ReportEntity> getAllReportEntity() {
		
		Session session = sessionFactory.openSession();
		
		List<ReportEntity> reports = null;
		
		try {
			reports = (List<ReportEntity>) session
					.createQuery("from ReportEntity order by timesp desc").list();

		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}

		logger.info("list of all ReportEntity successfully retrieved from the DB: "
				+ reports.toString());

		return reports;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<ReportEntity> getLastXReportEntity(int x) {
		
		Session session = sessionFactory.openSession();
		
		List<ReportEntity> reports = null;
		
		try {
			reports = (List<ReportEntity>) session
					.createQuery("SELECT * FROM bkkreport ORDER BY timesp DESC LIMIT " + x + ";")
					.list();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}

		logger.info("list of last " + x + " ReportEntity successfully retrieved from the DB: "
				+ reports.toString());

		return reports;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<ReportEntity> getAllRoutes() {
		
		Session session = sessionFactory.openSession();
		
		List<ReportEntity> reports = null;
		
		try {
			reports = (List<ReportEntity>) session
					.createQuery("from ReportEntity ORDER BY timesp")
					.list();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}

		logger.info("list all routes successfully retrieved from the DB: "
				+ reports.toString());

		return reports;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<ReportEntity> getAllStops() {
		
		Session session = sessionFactory.openSession();
		
		List<ReportEntity> reports = null;
		
		try {
			reports = (List<ReportEntity>) session
					.createQuery("SELECT stop FROM bkkreport ORDER BY timesp;")
					.list();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}

		logger.info("list of all stops successfully retrieved from the DB: "
				+ reports.toString());

		return reports;
	}
	
	@Override
	@Transactional
	public ReportEntity reportInspector(Integer route, Integer stop) {
		
		Session session = sessionFactory.openSession();
		
		ReportEntity report = new ReportEntity();
		report.setRoute(route);
		report.setStop(stop);
		report.autoTimeStamp();
		try {
			session.save(report);
			logger.info("Successfully insert new report in bkkreport database. '" + report.toString() + "'");
		} catch (HibernateException e){
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return report;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ReportEntity> getReportsByRouteAndYouth(Integer route,
			Integer hours) {

		Session session = sessionFactory.openSession();
		
		List<ReportEntity> result = null;
		
		// SELECT * FROM `bkkreport` where route = 1 AND TIMEDIFF(NOW(), timesp) < '01:00:00';
		String queryStr = "SELECT * FROM bkkreport "
				+ "WHERE "
				+ "route = " + route
				+ " AND ABS(TIMESTAMPDIFF(MINUTE, NOW(), timesp)) < " + hours + " * 60 ";
		
		try {
			result = session.createSQLQuery(queryStr).list();
			
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		logger.info("list of all ReportEntity successfully retrieved from the DB: "
				+ result.toString());
		return result;
	
	}



@SuppressWarnings("unchecked")
@Override
public List<BkkEntity> getPredictionForToday() {

		Session session = sessionFactory.openSession();
		
		List<BkkEntity> result = null;

		String queryStr = "SELECT " 
		+ " v.route_id as route_id,"
		+ " v.route as route," 
		+ " v.stop_id as stop_id,"
		+ " v.stop as stop, "
		+ " MAX(r.timesp) as timesp, "
		+ " v.type," 
		+ " v.longitude,"
		+ " v.latitude," 
		+ " COUNT(v.route_id) as frequency" 
		+ " FROM "
			+ "	bkkreport r," 
			+ "	bkk_v v" 
		+ " WHERE "
				+ "	r.route = v.route_id AND" 
				+ "	r.stop = v.stop_id AND"
				+ "	DAYOFWEEK(timesp) = DAYOFWEEK(NOW())"
				+ "	# 1 - sunday, 2 - monday, 3 - tuesday,"
				+ " # 4 - wednesday, 5 - thurday, 6 - friday, 7 - saturday"
		+ " GROUP BY " 
			+ " v.route_id," 
			+ "	v.route," 
			+ "	v.stop,"
			+ "	v.stop_id" 
		+ "ORDER BY cardinality desc;";
		
		try {
			result = (List<BkkEntity>) session.createSQLQuery(queryStr)
					.setResultTransformer(Transformers.aliasToBean(BkkEntity.class))
					.list();
			
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}

		return result;
	}
}
