package bkkwatcher.persistence.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import bkkwatcher.persistence.BkkDao;
import bkkwatcher.persistence.entity.BkkEntity;

/**
 * 
 *         IMPL (SUBLAYER): Here come the implementing classes of the
 *         persistence interfaces.
 * 
 */
public class BkkDaoImpl implements BkkDao {

	/**
	 * For logging DAO events.
	 */
	private Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 
	 * A method like this would normally run a Hibernate query to fill the
	 * entity from the database. In this example we didn't want to use a
	 * database so we 'wired' the results inside this class to imitate the
	 * query.
	 * 
	 */

	protected SessionFactory sessionFactory;

	@Required
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<BkkEntity> getAllBkkEntity() {
		Session session = sessionFactory.openSession();

		List<BkkEntity> bkkEntities = null;

		try {
			bkkEntities = (List<BkkEntity>) session.createQuery("from BkkEntity")
					.list();

		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}

		logger.info("BkkEntity successfully retrieved from the DB: "
				+ bkkEntities.toString());

		return bkkEntities;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<BkkEntity> getBkkEntityByRoute(int route_id) {
		Session session = sessionFactory.openSession();

		List<BkkEntity> bkkEntities = null;

		try {
			bkkEntities = (List<BkkEntity>) session
					.createSQLQuery("SELECT v.latitude,  v.longitude, v.route,"
									+ "  v.route_id, v.stop, "
									+ "  v.stop_id, v.type FROM bkk_v v "
									+ "WHERE v.route_id = :route_id "
									+ "ORDER BY v.order")
					.setInteger("route_id", route_id)
					.setResultTransformer(Transformers.aliasToBean(BkkEntity.class))
					.list();

		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}

		logger.info("BkkEntity successfully retrieved from the DB: "
				+ bkkEntities.toString());

		return bkkEntities;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<BkkEntity> getBkkEntityByStop(int stop_id) {
		Session session = sessionFactory.openSession();

		List<BkkEntity> bkkEntity = null;

		try {
			bkkEntity = (List<BkkEntity>) session
					.createSQLQuery("SELECT v.latitude,  v.longitude, v.route,"
									+ "  v.route_id, v.stop, "
									+ "  v.stop_id, v.type FROM bkk_v v "
									+ "WHERE v.stop_id = :stop_id")
					.setInteger("stop_id", stop_id)
					.setResultTransformer(Transformers.aliasToBean(BkkEntity.class))
					.list();

		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}

		logger.info("BkkEntity successfully retrieved from the DB: "
				+ bkkEntity.toString());

		return bkkEntity;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<BkkEntity> getNearbyBkkEntity(double longitude,
			double latitude, double distance) {

		Session session = sessionFactory.openSession();

		List<BkkEntity> nearbyEntities = null;
		try {
			String queryStr = "SELECT "
					+ "  type, "
					+ "  route_id, "
					+ "  route, "
					+ "  stop_id, "
					+ "  stop, "
					+ "  longitude, "
					+ "  latitude "
					+ "FROM "
					+ "( "
					+ "SELECT "
					+ "  type, "
					+ "  route_id, "
					+ "  route, "
					+ "  stop_id, "
					+ "  stop, "
					+ "  longitude, "
					+ "  latitude, "
					+ "  ROUND(((6367 * ACOS( ROUND( COS(RADIANS(90-0)) * COS(RADIANS(90-ABS(latitude - :lat))) + SIN(RADIANS(90-0)) * SIN(RADIANS(90-ABS(latitude - :lat))) * COS(RADIANS(0-ABS(longitude - :long))) ,15) )) * 1000), 2) AS distance "
					+ "FROM "
					+ "  bkk_v "
					+ "WHERE "
					+ "  ( "
					+ "    ( "
					+ "      :long < (longitude + (0 + (:distance_meter/(6378137*Cos(0))) * 180/PI())) "
					+ "    ) "
					+ "  AND "
					+ "    ( "
					+ "      :long > (longitude - (0 + (:distance_meter/(6378137*Cos(0))) * 180/PI())) "
					+ "    ) "
					+ "  ) "
					+ "AND "
					+ "  ( "
					+ "    ( "
					+ "      :lat < (latitude + (0 + (:distance_meter/6378137) * 180/PI())) "
					+ "    ) "
					+ "  AND "
					+ "    ( "
					+ "      :lat > (latitude - (0 + (:distance_meter/6378137) * 180/PI())) "
					+ "    ) " + "  ) " + "ORDER BY "
					+ "  distance ASC) as bkk_entity";

			nearbyEntities = (List<BkkEntity>) session
					.createSQLQuery(queryStr)
					.setFloat("lat", (float) latitude)
					.setFloat("long", (float) longitude)
					.setFloat("distance_meter", (float) distance)
					.setResultTransformer(Transformers.aliasToBean(BkkEntity.class))
					.list();

		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}

		logger.info("BkkEntity successfully retrieved from the DB: "
				+ nearbyEntities.toString());

		return nearbyEntities;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<BkkEntity> getNearbyReportedBkkEntity(double longitude,
			double latitude, double distance, int hours) {

		Session session = sessionFactory.openSession();

		List<BkkEntity> nearbyEntities = null;
		try {
			String queryStr = 
					" SELECT "
							+ "  v.route_id as route_id, "
							+ "  v.route as route, "
							+ "  v.stop_id as stop_id, "
							+ "  v.stop as stop, "
							+ "  MAX(r.timesp) as timesp, "
							+ "  v.type, "
							+ "  v.longitude, "
							+ "  v.latitude, "
							+ "  COUNT(v.route_id) as frequency "
							+ "FROM "
							+ "  bkkreport r, "
							+ "  bkk_v v "
							+ "WHERE "
							+ "  r.route                 = v.route_id "
							+ "AND r.stop                  = v.stop_id "
							+ "AND ABS(TIMESTAMPDIFF(MINUTE, NOW(), timesp)) < " + hours + " * 60 "
							+ "AND "
							+ "  ( "
							+ "    ( "
							+ "      ( "
							+ "        :long < (longitude + (0 + (:distance_meter/(6378137*Cos(0))) * 180/PI() "
							+ "        )) "
							+ "      ) "
							+ "    AND "
							+ "      ( "
							+ "        :long > (longitude - (0 + (:distance_meter/(6378137*Cos(0))) * 180/PI() "
							+ "        )) "
							+ "      ) "
							+ "    ) "
							+ "  AND "
							+ "    ( "
							+ "      ( "
							+ "        :lat < (latitude + (0 + (:distance_meter/6378137) * 180/PI())) "
							+ "      ) "
							+ "    AND "
							+ "      ( "
							+ "        :lat > (latitude - (0 + (:distance_meter/6378137) * 180/PI())) "
							+ "      ) "
							+ "    ) "
							+ "  ) "
							+ "GROUP BY "
							+ "  v.route_id, "
							+ "  v.route, "
							+ "  v.stop, "
							+ "  v.stop_id "
							+ "ORDER BY frequency desc";
			
			nearbyEntities = (List<BkkEntity>) session
					.createSQLQuery(queryStr)
					.setFloat("lat", (float) latitude)
					.setFloat("long", (float) longitude)
					.setFloat("distance_meter", (float) distance)
					.setResultTransformer(Transformers.aliasToBean(BkkEntity.class))
					.list();

		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}

		logger.info("BkkEntity successfully retrieved from the DB: "
				+ nearbyEntities.toString());

		return nearbyEntities;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<BkkEntity> getPredictionForDayOfWeek() {

		Session session = sessionFactory.openSession();

		List<BkkEntity> predictionEntities = null;
		try {
			String queryStr = 
					"SELECT "
							+ "  SUM_PER_STOPS_PER_DAY.route_id, "
							+ "  SUM_PER_STOPS_PER_DAY.route, "
							+ "  SUM_PER_STOPS_PER_DAY.stop_id, "
							+ "  SUM_PER_STOPS_PER_DAY.stop, "
							+ "  SUM_PER_STOPS_PER_DAY.type, "
							+ "  SUM_PER_STOPS_PER_DAY.longitude, "
							+ "  SUM_PER_STOPS_PER_DAY.latitude, "
							+ "  SUM_PER_STOPS_PER_DAY.frequency_per_stop_per_day AS frequency, "
							+ "  SUM_PER_DAY.frequency_per_day                    AS maxfrequency "
							+ "FROM "
							+ "  ( "
							+ "    SELECT "
							+ "      v.route_id AS route_id, "
							+ "      v.route    AS route, "
							+ "      v.stop_id  AS stop_id, "
							+ "      v.stop     AS stop, "
							+ "      v.type, "
							+ "      DAYOFWEEK(timesp) day, "
							+ "      v.longitude, "
							+ "      v.latitude, "
							+ "      COUNT(v.route_id) AS frequency_per_stop_per_day "
							+ "    FROM "
							+ "      bkkreport r, "
							+ "      bkk_v v "
							+ "    WHERE "
							+ "      r.route  = v.route_id "
							+ "    AND r.stop = v.stop_id "
							+ "    GROUP BY "
							+ "      v.route_id, "
							+ "      v.route, "
							+ "      v.stop_id, "
							+ "      v.stop, "
							+ "      DAY "
							+ "  ) AS SUM_PER_STOPS_PER_DAY "
							+ "LEFT JOIN "
							+ "  ( "
							+ "    SELECT "
							+ "      v.route_id        AS route_id, "
							+ "      v.route           AS route, "
							+ "      v.stop_id         AS stop_id, "
							+ "      v.stop            AS STOP, "
							+ "      COUNT(v.route_id) AS frequency_per_day "
							+ "    FROM "
							+ "      bkkreport r, "
							+ "      bkk_v v "
							+ "    WHERE "
							+ "      r.route  = v.route_id "
							+ "    AND r.stop = v.stop_id "
							+ "    GROUP BY "
							+ "      v.route_id, "
							+ "      v.route, "
							+ "      v.stop_id, "
							+ "      v.stop "
							+ "  ) AS SUM_PER_DAY "
							+ "ON "
							+ "  SUM_PER_DAY.route_id = SUM_PER_STOPS_PER_DAY.route_id "
							+ "AND SUM_PER_DAY.stop_id= SUM_PER_STOPS_PER_DAY.stop_id "
							+ "WHERE SUM_PER_STOPS_PER_DAY.day= DAYOFWEEK(NOW()) ";
			
			predictionEntities = (List<BkkEntity>) session
					.createSQLQuery(queryStr)
					.setResultTransformer(Transformers.aliasToBean(BkkEntity.class))
					.list();

		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}

		logger.info("BkkEntity prediction for today successfully retrieved from the DB: "
				+ predictionEntities.toString());

		return predictionEntities;
	}
}
