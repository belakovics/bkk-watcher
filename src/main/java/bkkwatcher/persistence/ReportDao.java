package bkkwatcher.persistence;

import java.util.List;

import bkkwatcher.persistence.entity.BkkEntity;
import bkkwatcher.persistence.entity.ReportEntity;

/**
 *
 * PERSISTENCE LAYER: 3rd layer.
 * 					  The business layer offers services to the upper layer via interface methods.
 * 					  This, combined with the dependency injection allows easy configuration and increases reusability.
 * 					  This layer handles the connection with the Database.
 * 
 * 					  DAO (Data Access Object) type services return single or a set of entities based on some given criteria.
 * 					  These are Hibernate entities that represent a Database record.
 *
 */
public interface ReportDao
{
	public ReportEntity reportInspector(Integer	route, Integer stop);
	
	public List<ReportEntity> getReportsByRouteAndYouth(Integer route, Integer hours);
	
	public List<ReportEntity> getAllReportEntity();	
	
	public List<ReportEntity> getLastXReportEntity(int x);
	
	public List<ReportEntity> getAllRoutes();	
	
	public List<ReportEntity> getAllStops();
	
	public List<BkkEntity> getPredictionForToday();
}
