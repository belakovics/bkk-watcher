package bkkwatcher.persistence;

import java.util.List;

import bkkwatcher.persistence.entity.BkkEntity;

/**
 *
 * PERSISTENCE LAYER: 3rd layer.
 * 					  The business layer offers services to the upper layer via interface methods.
 * 					  This, combined with the dependency injection allows easy configuration and increases reusability.
 * 					  This layer handles the connection with the Database.
 * 
 * 					  DAO (Data Access Object) type services return single or a set of entities based on some given criteria.
 * 					  These are Hibernate entities that represent a Database record.
 *
 */
public interface BkkDao
{
	public List<BkkEntity> getAllBkkEntity();
	public List<BkkEntity> getBkkEntityByRoute(int route_id);
	public List<BkkEntity> getBkkEntityByStop(int stop_id);
	public List<BkkEntity> getNearbyBkkEntity(double longitude, double latitude, double distance);
	public List<BkkEntity> getNearbyReportedBkkEntity(double longitude, double latitude, double distance, int hours);
	public List<BkkEntity> getPredictionForDayOfWeek();
}
